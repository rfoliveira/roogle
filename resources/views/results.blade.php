<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Roogle</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:200,600" rel="stylesheet" type="text/css">
        <link href="css/results.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main">
			<a class="logoLink" href="/">
				<img src="img/logo.png" class="logo"/>
			</a>
			<form method="get" action="/results">
				<div class="input">
					<input type="text" name="search" class="shadow" value="{{$currentSearch}}"/>
				</div>
			</form>
		</div>
		
		
		<div class="results">
			@foreach ($websites as $website)
				<div class="result">
					<h3 class="title"><a href="{{$website->url}}">{{$website->title}}</a></h3>
					<a href="{{$website->url}}">{{$website->url}}</a>
					<p class="description">{{$website->title}}</p>
				</div>
			@endforeach
		</div>
    </body>
</html>
