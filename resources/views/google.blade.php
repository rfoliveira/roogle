<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Roogle</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:200,600" rel="stylesheet" type="text/css">
        <link href="css/google.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main">
            <img src="img/logo.png" class="logo"/>
			<form method="get" action="/results">
				<div class="input">
					<input type="text" name="search" class="shadow"/>
				</div>
				<div class="buttons">
					<input type="submit" name="action" class="button" value="Roogle Search">
					<input type="submit" name="action" class="button" value="I'm Feeling Lucky">
				</div>
			</form>
		</div>
    </body>
</html>
