<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Website;

class WebsitesController extends Controller
{
	//
	

	public function searchWebsites(Request $request)
	{
		//Get search text from query string
	  $search=$request->input("search");
	  

	  //The first 'WHERE' is to check if there is a row with a title equal to the search text
	  $query=Website::where('title', $search); 
	  //split the search text in keywords to create de query by each keyword;
	  $keywords = explode(" ",$search);
	  foreach($keywords as $keyword){
		$query = $query->orWhere('title','like', '%' . $keyword . '%');  
		$query = $query->orWhere('keywords','like', '%' . $keyword . '%');  
	  }
	  //Order by rating
	  $query = $query->orderBy('rating', 'DESC');

	  $websites=$query->get();
	  //call the view results sending the data to the template.
	  if($request->input("action")=="I'm Feeling Lucky"){
		return redirect($websites[0]->url);
	  }else{
		return view('results')->with("websites", $websites)->with("currentSearch", $search);
	  }
	}
}
